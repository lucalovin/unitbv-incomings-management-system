﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Idk.ValidationHelper
{
    class ComboBoxRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var picked = value as String;
            if(picked == null){
                ValidationHelper.HasErrors = true;
                return new ValidationResult(false, "Please choose a value.");
            }
            ValidationHelper.HasErrors = false;
            return new ValidationResult(true, null);
        }
    }
}
