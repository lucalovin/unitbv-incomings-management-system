﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace Idk.ValidationHelper
{
    class DatePickerRule : ValidationRule
    {
        public int MinimumChars { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            DateTime time;
            if(!DateTime.TryParse((value ?? "").ToString(), CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out time))
            {
                ValidationHelper.HasErrors = true;
                return new ValidationResult(false, "Invalid date.");
            }
            ValidationHelper.HasErrors = false;
            return ValidationResult.ValidResult;
        }
    }
}
