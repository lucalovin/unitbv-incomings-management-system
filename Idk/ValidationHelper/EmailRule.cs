﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Idk.ValidationHelper
{
    class EmailRule : ValidationRule
    {
        public int MinimumChars { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(value.ToString());
            }
            catch
            {
                ValidationHelper.HasErrors = true;
                return new ValidationResult(false, "Invalid E-mail format.");
            }
            ValidationHelper.HasErrors = false;
            return ValidationResult.ValidResult;
        }
    }
}
