﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Idk.ValidationHelper
{
    class MinMaxRule : ValidationRule
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                int val = int.Parse(value as string, CultureInfo.InvariantCulture);

                if (val > MaxValue)
                {
                    ValidationHelper.HasErrors = true;
                    return new ValidationResult(false, $"Value exceeds maximum allowed value of {MaxValue}");
                }
                if (val < MinValue)
                {
                    ValidationHelper.HasErrors = true;
                    return new ValidationResult(false, $"Value cannot be less than {MinValue}");
                }
            }
            catch
            {
                ValidationHelper.HasErrors = true;
                return new ValidationResult(false, $"Value must be a number and positive");
            }
            ValidationHelper.HasErrors = false;
            return ValidationResult.ValidResult;
        }
    }
}
