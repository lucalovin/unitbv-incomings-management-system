﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Idk.ValidationHelper
{
    class MinimumCharsRule : ValidationRule
    {
        public int MinimumChars { get; set; }
        public int MaxChars { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            String val = value as String;

            if (String.IsNullOrWhiteSpace(val))
            {
                ValidationHelper.HasErrors = true;
                return new ValidationResult(false, "Value cannot be empty.");
            }
            if (val.Length < MinimumChars)
            {
                ValidationHelper.HasErrors = true;
                return new ValidationResult(false, $"Value must be at least {MinimumChars} chars long.");
            }
            if (val.Length > MaxChars && MaxChars != 0)
            {
                ValidationHelper.HasErrors = true;
                return new ValidationResult(false, $"Value must not contain more than {MaxChars} chars.");
            }
            ValidationHelper.HasErrors = false;
            return ValidationResult.ValidResult;
        }
    }
}
