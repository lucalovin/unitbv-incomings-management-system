﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idk
{
    public class Client
    {
        public String FullName { get; set; }
		public String FirstName { get; set; }
		public String MiddleName { get; set; }
		public String LastName { get; set; }
		public String Address { get; set; }
		public String Email { get; set; }
		public String DateRegistered { get; set; }
        public String SesKey { get; set; }
        public String Gender { get; set; }
        public int ClientID { get; set; }
        public int Age { get; set; }
        public int Rank { get; set; }
        public int FirstVisit { get; set; }
        public long CNP { get; set; }
       
		public Client() { }
	
        public Client(String fullname, int id, int age, int gender, String addr, int rank)
        {
            if (fullname.Split(' ').Length >= 3)
            {
				FirstName = fullname.Split(' ')[0];
				MiddleName = fullname.Split(' ')[1];
				LastName = fullname.Split(' ')[2];
            }
            else
            {
                try
                { 
                    FirstName = fullname.Split(' ')[0];
                    LastName = fullname.Split(' ')[1];
                }
                catch (System.IndexOutOfRangeException)
                {
                    if(fullname.Split(' ').Length == 1) {
                        FirstName = fullname.Split(' ')[0];
                    }
                    Console.WriteLine("Name is too short.");
                }
            }
			FullName = fullname;
			ClientID = id;
			Age = age;
            Gender = ((gender == 1) ? "Male" : "Female");
			Address = addr;
			Rank = rank;
        }

        public String getName(int n)
        {
            switch (n)
            {
                case 0: return FirstName; //returns first name
                case 1: return MiddleName; //returns middle name
                case 2: return LastName; //returns last name
                default: return FullName;
            }
        }

        public String getTextRank()
        {
            switch (Rank)
            {
                case 0: return "Student";
                case 1: return "Teacher";
                case 2: return "Administrator";
                case 5: return "The Best Around";
                default: return "Student";
            }
        }
    }
}
