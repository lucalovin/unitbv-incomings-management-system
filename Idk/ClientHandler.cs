﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using CryptSharp;
using TimetableReading;
using System.IO;
using System.Reflection;
using System.Windows;

namespace Idk
{
    public class ClientHandler
    {

        private static Client client;
        private static readonly HttpClient req = new HttpClient();

        /// <summary>
        /// Async method to remotely verify user details.
        /// </summary>
        /// <param name="email">User E-mail</param>
        /// <param name="password">User Password</param>
        /// <returns>True on successful validation, false on incorrect details or server error.</returns>
        public static async Task<bool> authenticateAsync(String email, String password)
        {
            if (email.Length > 0 && email != null && password.Length > 0 && password != null)
            {
                var values = new Dictionary<String, String> {
                   { "email", email },
                   { "password", password }
                };

                var content = new FormUrlEncodedContent(values);

                var response = await req.PostAsync("https://bamalex.com/api/auth_v3.php", content);

                var responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
                dynamic res = JObject.Parse(responseString);

                if (res["result"].ToObject<String>() == "OK") {
                    try
                    {
                        var details = res["details"];
                        client = new Client(details["fullname"].ToObject<String>(), details["id"].ToObject<int>(), details["age"].ToObject<int>(), details["gender"].ToObject<int>(), details["address"].ToObject<String>(), details["rank"].ToObject<int>());
                        client.Email = details["email"].ToObject<String>();
                        client.CNP = details["CNP"].ToObject<long>();
                        client.DateRegistered = details["registered"].ToObject<String>();
                        client.SesKey = res["seskey"].ToObject<String>();
                        client.FirstVisit = details["firstVisit"].ToObject<int>();
                        response = await req.GetAsync("https://bamalex.com/api/student_data_v2.php?client=" + ClientHandler.getClient().ClientID + "&seskey=" + ClientHandler.getClient().SesKey);
                        responseString = await response.Content.ReadAsStringAsync();
                        try
                        {
                            res = JObject.Parse(responseString);
                            if (res["result"].ToObject<String>() == "OK")
                            {
                                var students = res["students"];
                                foreach (JObject item in students)
                                {
                                    Student st = new Student(item.GetValue("fullname").ToObject<String>(), item.GetValue("id").ToObject<int>(), item.GetValue("age").ToObject<int>(), item.GetValue("gender").ToObject<int>(), item.GetValue("address").ToObject<String>(), 0, item.GetValue("year").ToObject<int>());
                                    st.Groupname = item.GetValue("group").ToObject<String>();
                                    st.Faculty = item.GetValue("faculty").ToObject<String>();
                                    st.Specialization = item.GetValue("spec").ToObject<String>();
                                    st.Email = item.GetValue("email").ToObject<String>();
                                    st.Teacher = item.GetValue("origin_teacher_contact").ToObject<String>();
                                    st.Country = item.GetValue("country").ToObject<String>();
                                    st.UniverityOfOrigin = item.GetValue("origin_univ").ToObject<String>();
                                    try
                                    {
                                        foreach (JObject course in item["courses"])
                                        {
                                              st.AttendingCourses.Add(new SubjectInfo(course.GetValue("Key").ToObject<string>(), course.GetValue("Year").ToObject<int>(), course.GetValue("Type").ToObject<string>(), course.GetValue("Room").ToObject<string>(), course.GetValue("Professor").ToObject<string>(), course.GetValue("Day").ToObject<string>(), course.GetValue("Hours").ToObject<string>(), course.GetValue("Parity").ToObject<string>(), course.GetValue("Group").ToObject<string>(), course.GetValue("Semigroup").ToObject<string>()));
                                            //st.Courses.Add(course.GetValue("name").ToObject<String>(), course.GetValue("grade").ToObject<int>());
                                        }
                                    }
                                    catch (InvalidCastException)
                                    {
                                        Console.WriteLine("Incorrect course format.");
                                    }
                                    StudentHandler.addStudent(st);
                                }
                            }
                            return true;
                        }
                        catch (IndexOutOfRangeException)
                        {
                            Console.WriteLine("Could not fetch data.");
                        }
                    }
                    catch (Newtonsoft.Json.JsonReaderException) {
                        Console.WriteLine("A server-side error occured: " + responseString);
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Updates student data on the remote database. 
        /// </summary>
        /// <param name="student">JSON string containing a student object.</param>
        /// <returns>True on success, False on failure.</returns>
        public static async Task<bool> UpdateRemote(String student, bool remove)
        {
            var values = new Dictionary<String, String> {
                   { "clientID", ClientHandler.getClient().ClientID.ToString() },
                   { "seskey", ClientHandler.getClient().SesKey },
                   { "student", student },
                   { "delete", remove.ToString() }
                };

            var content = new FormUrlEncodedContent(values);

            var response = await req.PostAsync("https://bamalex.com/api/update_data_v2.php", content);

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseString);
            try {
                dynamic res = JObject.Parse(responseString);
                switch (res["result"].ToObject<String>()) {
                    case "OK": return true;
                    case "NEW_OK": return true;
                    case "NOT_OK": Console.WriteLine(responseString + ": " + res["msg"]); return false;
                    default: Console.WriteLine(responseString + ": " + res["msg"]); return false;
                }
            } catch 
            {
                string msgtext = "Server error. Please try again.";
                string txt = "Connection Failed";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxResult result = MessageBox.Show(msgtext, txt, button, MessageBoxImage.Exclamation);
                Console.WriteLine(responseString);
                return false;
            }
        }

        public static List<String> GetCountries()
        {
            
            string lines = File.ReadAllText(@"Resources/countries.json");
            for(int i = 0; i < lines.Length; i++)
            {
                Console.WriteLine(lines[i]);
            }
            try
            {
                Console.WriteLine(lines.ToString());
                dynamic res = JArray.Parse(lines.ToString());
                Console.WriteLine(res);
                List<String> c = new List<String>();
                foreach (JObject country in res)
                {
                    c.Add(country.GetValue("name").ToObject<String>());
                }
                return c;
            }
            catch
            {
                Console.WriteLine("Could not load countries.json");
                return null;
            }
        }

        /// <summary>
        /// Returns the Client object containing the info of the currently authenticated user.
        /// </summary>
        /// <returns>Client object</returns>
        public static Client getClient()
        {
            return client;
        }

    }
}
