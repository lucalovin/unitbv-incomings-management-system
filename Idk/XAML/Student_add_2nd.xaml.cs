﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using TimetableReading;

namespace Idk.XAML
{
    /// <summary>
    /// Interaction logic for Student_add_2nd.xaml
    /// </summary>
    public partial class Student_add_2nd : Page
    {
        private readonly ObservableCollection<SubjectsView> _subjects;
        private static ObservableCollection<SubjectsView> subjectsToSelect;
        private static Reader r;
        private static Student _student;
        private static Student_add_2nd instance { set; get; }
        public Student_add_2nd(Student st)
        {
            _student = st;
            instance = this;
            InitializeComponent();
            this.DataContext = this;
            Loaded += Student_add_2nd_Loaded;
            Console.WriteLine(st.FullName);
            _subjects = CreateData();
        }

        private void Student_add_2nd_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationService.RemoveBackEntry();
        }

        public Student_add_2nd()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private static ObservableCollection<SubjectsView> CreateData()
        {
            List<string> Keys = new List<string>();
            subjectsToSelect = new ObservableCollection<SubjectsView>();
            r = new Reader();
            try
            {
                if (_student.AttendingCourses.Count > 0)
                {
                    foreach (SubjectInfo si in _student.AttendingCourses)
                    {
                        Keys.Add(si.Key);
                    }
                }
            }
            catch
            {

            }
            foreach (KeyValuePair<string, List<SubjectInfo>> item in r.GetResult())
            {
                    if (Keys.Contains(item.Key))
                    {
                        subjectsToSelect.Add(new SubjectsView { Code = item.Key[0], Name = item.Key, Description = "Year: " + item.Value[0].Year, IsSelected = true });
                    }
                    else
                    {
                        subjectsToSelect.Add(new SubjectsView { Code = item.Key[0], Name = item.Key, Description = "Year: " + item.Value[0].Year, IsSelected = false });
                    }
              }
            return subjectsToSelect;
        }

        public ObservableCollection<SubjectsView> SelectableSubjects => _subjects;

        public static void SaveStudentTimetable(object sender, RoutedEventArgs e)
        {
            _student.AttendingCourses.Clear();
            List<string> s = new List<string>();
            foreach(SubjectsView item in subjectsToSelect)
            {
                if (item.IsSelected)
                {
                    s.Add(item.Name);
                }
            }
            foreach(KeyValuePair<string, List<SubjectInfo>> item in r.GetSpecificSubjectsDictionary(s))
            {
                foreach(SubjectInfo si in item.Value)
                {
                    _student.AttendingCourses.Add(si);
                }
            }
            StudentHandler.SaveStudent(_student, false);
            instance.NavigationService.Navigate(new ViewTimetable(_student));
        }

      }
}
