﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Navigation;

namespace Idk
{
    /// <summary>
    /// Interaction logic for Home_Page.xaml
    /// </summary>
    public partial class Home_Page : Page
    {
        private static bool isInit = false;

        public Home_Page()
        {
            InitializeComponent();
            Loaded += init;
            Unloaded += Home_Page_Unloaded;
        }

        private void Home_Page_Unloaded(object sender, RoutedEventArgs e)
        {
             GC.Collect();
             GC.WaitForPendingFinalizers();
        }

        void init(object sender, RoutedEventArgs e)
        {
            NavigationService.RemoveBackEntry();
            UserName.Text = $"Logged in as {ClientHandler.getClient().FullName}";
            if (!isInit)
            {
                DoubleAnimation animation = new DoubleAnimation();
                animation.To = 1;
                animation.From = 0;
                animation.Duration = TimeSpan.FromMilliseconds(3000);
                animation.EasingFunction = new QuadraticEase();

                Storyboard sb = new Storyboard();
                sb.Children.Add(animation);

                Storyboard.SetTarget(sb, logo);
                Storyboard.SetTargetProperty(sb, new PropertyPath(Control.OpacityProperty));

                sb.Begin();
                isInit = true;
            }
            if (ClientHandler.getClient().FirstVisit == 1)
            {
                Home.ToggleMenu();
            }
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
