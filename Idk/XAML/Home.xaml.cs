﻿using Idk.XAML;
using MaterialDesignThemes.Wpf;
using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using WinInterop = System.Windows.Interop;

namespace Idk
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Window
    {
        public static bool isInit = false;
        public static Home instance { get; set; }
        public Home()
        {
            InitializeComponent();
            this.SourceInitialized += new EventHandler(win_SourceInitialized);
            this.Loaded += HomeLoaded;
        }

        private void HomeLoaded(object sender, RoutedEventArgs e)
        {
            TimetableReading.TimetableHelper.GetLatestTimetable();
            if (ClientHandler.getClient().FirstVisit == 1) { 
                HomeFrame.Content = new Intro();
            }
            else
            {
                HomeFrame.Content = new Home_Page();
            }
            username.Text = ClientHandler.getClient().FullName;
            email.Text = ClientHandler.getClient().Email;
            WindowTitle.Text = "UniTBV Erasmus+ ISMS (v0.0.5 A)"; //Define this somewhere else later
            instance = this;
            if(ClientHandler.getClient().FirstVisit == 1)
            {
                ToggleMenu();
            }
        }

        #region Avoid hiding task bar upon maximalisation (src https://stackoverflow.com/questions/6890472/wpf-maximize-window-with-windowstate-problem-application-will-hide-windows-ta)

        private static System.IntPtr WindowProc(
              IntPtr hwnd,
              int msg,
              IntPtr wParam,
              IntPtr lParam,
              ref bool handled)
        {
            switch (msg)
            {
                case 0x0024:
                    WmGetMinMaxInfo(hwnd, lParam);
                    handled = true;
                    break;
            }

            return (System.IntPtr)0;
        }

        void win_SourceInitialized(object sender, EventArgs e)
        {
            IntPtr handle = (new WinInterop.WindowInteropHelper(this)).Handle;
            WinInterop.HwndSource.FromHwnd(handle).AddHook(new WinInterop.HwndSourceHook(WindowProc));
        }

        private static void WmGetMinMaxInfo(System.IntPtr hwnd, System.IntPtr lParam)
        {

            MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

            // Adjust the maximized size and position to fit the work area of the correct monitor
            int MONITOR_DEFAULTTONEAREST = 0x00000002;
            IntPtr monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

            if (monitor != System.IntPtr.Zero)
            {

                MONITORINFO monitorInfo = new MONITORINFO();
                GetMonitorInfo(monitor, monitorInfo);
                RECT rcWorkArea = monitorInfo.rcWork;
                RECT rcMonitorArea = monitorInfo.rcMonitor;
                mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left);
                mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top);
                mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left);
                mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
            }

            Marshal.StructureToPtr(mmi, lParam, true);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            /// <summary>
            /// x coordinate of point.
            /// </summary>
            public int x;
            /// <summary>
            /// y coordinate of point.
            /// </summary>
            public int y;

            /// <summary>
            /// Construct a point of coordinates (x,y).
            /// </summary>
            public POINT(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MINMAXINFO
        {
            public POINT ptReserved;
            public POINT ptMaxSize;
            public POINT ptMaxPosition;
            public POINT ptMinTrackSize;
            public POINT ptMaxTrackSize;
        };

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class MONITORINFO
        {
            /// <summary>
            /// </summary>            
            public int cbSize = Marshal.SizeOf(typeof(MONITORINFO));

            /// <summary>
            /// </summary>            
            public RECT rcMonitor = new RECT();

            /// <summary>
            /// </summary>            
            public RECT rcWork = new RECT();

            /// <summary>
            /// </summary>            
            public int dwFlags = 0;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 0)]
        public struct RECT
        {
            /// <summary> Win32 </summary>
            public int left;
            /// <summary> Win32 </summary>
            public int top;
            /// <summary> Win32 </summary>
            public int right;
            /// <summary> Win32 </summary>
            public int bottom;

            /// <summary> Win32 </summary>
            public static readonly RECT Empty = new RECT();

            /// <summary> Win32 </summary>
            public int Width
            {
                get { return Math.Abs(right - left); }  // Abs needed for BIDI OS
            }
            /// <summary> Win32 </summary>
            public int Height
            {
                get { return bottom - top; }
            }

            /// <summary> Win32 </summary>
            public RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }


            /// <summary> Win32 </summary>
            public RECT(RECT rcSrc)
            {
                this.left = rcSrc.left;
                this.top = rcSrc.top;
                this.right = rcSrc.right;
                this.bottom = rcSrc.bottom;
            }

            /// <summary> Win32 </summary>
            public bool IsEmpty
            {
                get
                {
                    // BUGBUG : On Bidi OS (hebrew arabic) left > right
                    return left >= right || top >= bottom;
                }
            }
            /// <summary> Return a user friendly representation of this struct </summary>
            public override string ToString()
            {
                if (this == RECT.Empty) { return "RECT {Empty}"; }
                return "RECT { left : " + left + " / top : " + top + " / right : " + right + " / bottom : " + bottom + " }";
            }

            /// <summary> Determine if 2 RECT are equal (deep compare) </summary>
            public override bool Equals(object obj)
            {
                if (!(obj is Rect)) { return false; }
                return (this == (RECT)obj);
            }

            /// <summary>Return the HashCode for this struct (not garanteed to be unique)</summary>
            public override int GetHashCode()
            {
                return left.GetHashCode() + top.GetHashCode() + right.GetHashCode() + bottom.GetHashCode();
            }


            /// <summary> Determine if 2 RECT are equal (deep compare)</summary>
            public static bool operator ==(RECT rect1, RECT rect2)
            {
                return (rect1.left == rect2.left && rect1.top == rect2.top && rect1.right == rect2.right && rect1.bottom == rect2.bottom);
            }

            /// <summary> Determine if 2 RECT are different(deep compare)</summary>
            public static bool operator !=(RECT rect1, RECT rect2)
            {
                return !(rect1 == rect2);
            }


        }

        [DllImport("user32")]
        internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("User32")]
        internal static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ListViewItem btn = sender as ListViewItem;
            switch (btn.Name) {
                case "btn_home":
                    HomeFrame.Content = new Home_Page();
                    break;
                case "btn_viewstudents":
                    HomeFrame.Content = new Students();
                    break;
                case "btn_mng":
                   // HomeFrame.Content = new Manage();
                    break;
                case "btn_settings":
                    HomeFrame.Content = new Settings();
                    break;
                case "btn_add_student":
                    HomeFrame.Content = new Students_Add();
                    break;
                default:
                    HomeFrame.Content = new Home_Page();
                    break;
            }
        }
        protected void ToggleWindowState()
        {
            if (base.WindowState != WindowState.Maximized)
            {
                base.WindowState = WindowState.Maximized;
            }
            else
            {
                base.WindowState = WindowState.Normal;
            }
        }

        private void beginDrag(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Left)
            {
                DragMove();
            }
        }

        private void exitWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void resize(object sender, RoutedEventArgs e)
        {
            this.ToggleWindowState();
        }

        private void minimize(object sender, RoutedEventArgs e)
        {
            base.WindowState = WindowState.Minimized;
        }

        private void Btn_forms_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
             new PaletteHelper().SetLightDark(true);
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            switch (MenuButton.IsChecked)
            {
                case true:
                    Menu.Visibility = Visibility.Visible;
                    Menu.ToolTip = "Hide Menu";
                    break;
                case false:
                    Menu.Visibility = Visibility.Collapsed;
                    Menu.ToolTip = "Show Menu";
                    break;
                default: break;
            }
        }

        public static void ToggleMenu()
        {
            switch (Home.instance.MenuButton.IsChecked)
            {
                case true:
                    Home.instance.Menu.Visibility = Visibility.Collapsed;
                    Home.instance.MenuButton.IsChecked = false;
                    break;
                case false:
                    Home.instance.Menu.Visibility = Visibility.Visible;
                    Home.instance.MenuButton.IsChecked = true;
                    break;
                default: break;
                }
        }
    }
}
