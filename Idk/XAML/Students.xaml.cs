﻿using Idk.XAML;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Idk
{
    /// <summary>
    /// Interaction logic for Students.xaml
    /// </summary>
    public partial class Students : Page
    {
        public static ObservableCollection<Student> Stud { get; set; }
        private static DataGrid std;
        private static MenuItem DeleteStudentMenuItem;
        public Students()
        {
            Stud = new ObservableCollection<Student>();
            InitializeComponent();
            this.DataContext = this;
            Loaded += Students_Loaded;
            Unloaded += Students_Unloaded;
            std = StudentsView;
            DeleteStudentMenuItem = context_menu_delete;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Students_Unloaded(object sender, RoutedEventArgs e)
        {
            
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void Students_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationService.RemoveBackEntry();
            foreach (Student item in StudentHandler.getStudents().Values)
            {
                Stud.Add(item);
            }
        }

        public IEnumerable<string> Genders
        {
            get
            {
                yield return "Male";
                yield return "Female";
            }
        }

        /// <summary>
        /// Makes the GridView from the "View Students" menu editable.
        /// </summary>
        /// <param name="r">False: Enables edting, True: Disables editing</param>
        public static void setReadOnly(bool r)
        {
            std.IsReadOnly = r;
            DeleteStudentMenuItem.IsEnabled = !r;
        }

        private void StudentsView_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            // Only mark edited students, so that we don't send all students to the server. Saves bandwidth and takes less time.
            Student st = (Student)StudentsView.SelectedItem;
            st.Modified = true;
        }

        public static void SaveEdit()
        {
            int i = 0;
            foreach(KeyValuePair<int, Student> entry in StudentHandler.getStudents())
            {
                if(entry.Value.Modified == true)
                {
                    i++;
                    StudentHandler.SaveStudent(entry.Value, false);
                    entry.Value.Modified = false; //reset modified state so that we dont send them again the next time we edit something
                }
            }
            Console.WriteLine($"{i} students updated.");
        }

        private void DeleteStudent(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;
            
            var contextMenu = (ContextMenu)menuItem.Parent;
               
            var item = (DataGrid)contextMenu.PlacementTarget;

            if (item.SelectedItems.Count > 1)
            {
                for (int i = 0; i < item.SelectedItems.Count; i++)
                {
                    Stud.Remove((Student)item.SelectedItems[i]);
                    StudentHandler.RemoveStudent((Student)item.SelectedItems[i]);
                }
            }
            else
            {
                StudentHandler.RemoveStudent((Student)item.SelectedItems[0]);
                Stud.Remove((Student)item.SelectedItems[0]);
            }
        }

        private void ViewTimeTable(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;

            var contextMenu = (ContextMenu)menuItem.Parent;

            var item = (DataGrid)contextMenu.PlacementTarget;
       
            NavigationService.Navigate(new ViewTimetable((Student)item.SelectedCells[0].Item));
        }

        private void EditTimetable(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;

            var contextMenu = (ContextMenu)menuItem.Parent;

            var item = (DataGrid)contextMenu.PlacementTarget;

            NavigationService.Navigate(new Student_add_2nd((Student)item.SelectedCells[0].Item));
        }

        private void OnDeletePressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Delete && std.IsReadOnly == false)
            {
                DataGrid grid = (DataGrid)sender;
                if(grid.SelectedCells.Count > 1)
                {
                    for (int i = 0; i < grid.SelectedCells.Count; i++)
                    {
                        Stud.Remove((Student)grid.SelectedCells[i].Item);
                        StudentHandler.RemoveStudent((Student)grid.SelectedCells[i].Item);
                    }
                }
                else
                {
                    StudentHandler.RemoveStudent((Student)grid.SelectedCells[0].Item);
                    Stud.Remove((Student)grid.SelectedCells[0].Item);
                }

            }
        }
    }
}
