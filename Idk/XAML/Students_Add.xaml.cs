﻿using Idk.ValidationHelper;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Idk.XAML
{
    /// <summary>
    /// Interaction logic for Students_Add.xaml
    /// </summary>
    public partial class Students_Add : Page
    { 

        public String student_address { set; get; }
        public String student_fullname { set; get; }
        public String student_dob { set; get; }
        public String student_gender { set; get; }
        public String student_cnp { set; get; }
        public String student_country { set; get; }
        public String student_unioforigin { set; get; }
        public String student_destinationUni { set; get; }
        public String student_destinationFaculty { set; get; }
        public String student_destinationSpecialization { set; get; }
        public String student_faculty { set; get; }
        public String student_year { set; get; }
        public String student_email { set; get; }
        public String student_group { set; get; }
        public String student_teacher { set; get; }
        private Student st;
        public static SnackbarMessageQueue snackBarQueue;
        public static ObservableCollection<String> Countries { get; set; }
        public static ObservableCollection<String> Faculties { get; set; }

        public Students_Add()
        {
            Countries = new ObservableCollection<String>();
            Faculties = new ObservableCollection<String>();
            InitializeComponent();
            this.DataContext = this;
            Loaded += Students_Add_Loaded;
        }

        private void Students_Add_Loaded(object sender, RoutedEventArgs e)
        {
            snackBarQueue = snackBar.MessageQueue;
            if (!TimetableReading.TimetableHelper.Status && !TimetableReading.TimetableHelper.DownloadStatus)
            {
                var messageQueue = snackBar.MessageQueue;
                var message = "Latest Timetable data could not be loaded.";
                Task.Factory.StartNew(() => messageQueue.Enqueue(message));
            }
            List<string> _countries = ClientHandler.GetCountries();
            foreach (var country in _countries)
            {
                Countries.Add(country);
            }
            Faculties.Add("Inginerie Electrica si Stiinta Calc.");
            Faculties.Add("Calculatoare");
            Faculties.Add("Robotica");
            new_student_destinationUniverity.IsEnabled = false;
            new_student_destinationUniverity.Text = "Transilvania University of Brasov";
        }

        private void New_student_dob_CalendarClosed(object sender, RoutedEventArgs e)
        {
            String date = new_student_dob.SelectedDate.ToString();
            try {
                int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                int bod = int.Parse(DateTime.Parse(date).ToString("yyyyMMdd"));
                int age = (now - bod) / 10000;
                if (age > 18 && age <= 50)
                {
                    //MaterialDesignThemes.Wpf.HintAssist.SetHint(new_student_age, age);
                    new_student_age.Text = age.ToString();
                }
            } catch (System.FormatException)
            {
                
            }
        }

        private void New_student_dob_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            String date = new_student_dob.Text;
            try
            {
                int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                int bod = int.Parse(DateTime.Parse(date).ToString("yyyyMMdd"));
                int age = (now - bod) / 10000;
                if (age > 18 && age <= 50)
                {
                    //MaterialDesignThemes.Wpf.HintAssist.SetHint(new_student_age, age);
                    new_student_age.Text = age.ToString();
                }
            }
            catch (System.FormatException)
            {

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ResetForm();
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        private void SaveStudent(object sender, RoutedEventArgs e)
        {
            if (!ValidationHelper.ValidationHelper.HasErrors)
            {
                try
                {
                    st = new Student(new_student_fullname.Text, StudentHandler.getStudents().Keys.LastOrDefault() + 1, int.Parse(new_student_age.Text), (int)new_student_gender.SelectedIndex-1, new_student_address.Text, 0, 0);
                    st.DateOfBirth = new_student_dob.SelectedDate.ToString();
                    st.UniverityOfOrigin = new_student_universityoforigin.Text;
                    st.Specialization = new_student_destinationSpecialization.Text;
                    st.Faculty = new_student_faculty.Text;
                    st.University = new_student_destinationUniverity.Text;
                    st.Country = new_student_country.Text;
                    st.CNP = long.Parse(new_student_ID.Text);
                    st.Email = new_student_email.Text;
                    st.Groupname = "0";
                    st.Teacher = new_student_teacher.Text;
                    if (StudentHandler.SaveStudent(st, true))
                    {
                        snackBar.Background = Brushes.Green;
                        snackBar.Foreground = Brushes.White;
                        var messageQueue = snackBar.MessageQueue;
                        var message = "Student data for " + st.FullName + " saved.";
                        Task.Factory.StartNew(() => messageQueue.Enqueue(message));
                        NavigationService.Navigate(new Student_add_2nd(st));
                    }
                    else
                    {
                        snackBar.Background = Brushes.Red;
                        snackBar.Foreground = Brushes.White;
                        var messageQueue = snackBar.MessageQueue;
                        var message = "Student data for " + st.FullName + " could not be saved.";
                        Task.Factory.StartNew(() => messageQueue.Enqueue(message));
                    }
                }
                catch
                {
                    Console.WriteLine("Something went wrong.");
                }
            }
            else
            {
                var messageQueue = snackBar.MessageQueue;
                var message = "Form has errors!";
                Task.Factory.StartNew(() => messageQueue.Enqueue(message));
            }
        }

        private void ResetForm()
        {
             foreach (TextBox tb in FindVisualChildren<TextBox>(this))
             {
                 tb.Text = "";
             }
        }

        private void NumericInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+");
            return !regex.IsMatch(text);
        }

        private void DisablePasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text))
                {
                    e.CancelCommand();
                }
            }
            else
            {
                e.CancelCommand();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
 
        }
    }
}
