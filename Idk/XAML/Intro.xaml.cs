﻿using System.Windows;
using System.Windows.Controls;


namespace Idk
{
    /// <summary>
    /// Interaction logic for Messages.xaml
    /// </summary>
    public partial class Intro : Page
    {
        public Intro()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Home_Page());
        }
    }
}
