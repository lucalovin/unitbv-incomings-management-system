﻿using MaterialDesignThemes.Wpf;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Idk.XAML
{
    /// <summary>
    /// Interaction logic for TaskBar.xaml
    /// </summary>
    public partial class TaskBar : UserControl
    {
        public static readonly DependencyProperty CurrentLocationProperty = DependencyProperty.Register("ParentLocation", typeof(string), typeof(TaskBar));
        public static readonly DependencyProperty ChildLocationProperty = DependencyProperty.Register("ChildLocation", typeof(string), typeof(TaskBar));
        public static readonly DependencyProperty SecondChildProperty = DependencyProperty.Register("SecondChild", typeof(string), typeof(TaskBar));
        public static string StudentName { set; get; }

        public string ParentLocation
        {
            get { return (string)GetValue(CurrentLocationProperty); }
            set { SetValue(CurrentLocationProperty, value); }
        }

        public string ChildLocation
        {
            get { return (string)GetValue(ChildLocationProperty); }
            set { SetValue(ChildLocationProperty, value); }
        }

        public string SecondChild
        {
            get { return (string)GetValue(SecondChildProperty); }
            set { SetValue(SecondChildProperty, value); }
        }

        public TaskBar()
        {
            InitializeComponent();
            this.DataContext = this;
            Loaded += Initialize;
        }

        private void Initialize(object sender, RoutedEventArgs e)
        {
            ParentLocation = ParentLocation ?? "Home";
            ParentIcon.Kind = GetParentIcon();
            switch (ParentLocation.ToLower())
            {
                case "view students":
                    Students_Edit.Visibility = Visibility.Visible;
                    break;
                case "manage students":
                    if(SecondChild == "Generate Student Timetable")
                    {
                        save_timeline_group.Visibility = Visibility.Visible;
                        save_timetable.Visibility = Visibility.Visible;
                        save_timetable.Click += Student_add_2nd.SaveStudentTimetable;
                    }
                    if (SecondChild == "View Timetable")
                    {
                        print_timetable_group.Visibility = Visibility.Visible;
                        print_timetable.Click += ViewTimetable.OnPrintClicked;
                        edit_timetable.Click += ViewTimetable.OnEditClicked;
                    }
                    break;
                /*case "add new student":
                    Students_Save_New.Visibility = Visibility.Visible;
                    break;*/ //Maybe a future update where buttons are in the taskbar, not at the buttom of the form
                default: break;
            }
            ParentLocation = (ParentLocation != null && ChildLocation != null) ? ParentLocation + " > " : ParentLocation;
            if (ChildLocation != null)
            {
                ChildIcon.Kind = GetChildIcon();
            }
            else
            {
                ChildIcon.Visibility = Visibility.Collapsed;
            }
            if (SecondChild != null && ChildLocation != null)
            {
                if(SecondChild == "View Timetable")
                {
                    ChildIcon.Kind = PackIconKind.User;
                }
                ChildLocation += " > ";
                SecondChildIcon.Kind = GetSecondChildIcon();
            }
            else
            {
                SecondChildIcon.Visibility = Visibility.Collapsed;
            }

        }

        private void SwitchEditMode(object sender, RoutedEventArgs e)
        {
            ToggleButton tb = sender as ToggleButton;
            switch (tb.IsChecked)
            {
                case true:
                    Students.setReadOnly(false);
                    toggleEdit.Background = Brushes.Yellow;
                    txt_toggleEdit.Text = "Stop Editing";
                    break;
                case false:
                    Students.SaveEdit();
                    Students.setReadOnly(true);
                    toggleEdit.Background = Brushes.Green;
                    txt_toggleEdit.Text = "Start Editing";
                    break;
                default: break;
            }
        }

        private PackIconKind GetChildIcon()
        {
            switch (ChildLocation.ToLower())
            {
                case "add new student":
                    return PackIconKind.AccountPlus;
                case "edit student":
                    return PackIconKind.UserEdit;
                default: return PackIconKind.Null;
            }
        }

        private PackIconKind GetSecondChildIcon()
        {
            switch (SecondChild.ToLower())
            {
                case "generate student timetable":
                    return PackIconKind.Timetable;
                case "view timetable":
                    return PackIconKind.Timetable;
                default: return PackIconKind.Null;
            }
        }

        private PackIconKind GetParentIcon()
        {
            switch (ParentLocation.ToLower())
            {
                case "home":
                    return PackIconKind.Home;
                case "manage students":
                    return PackIconKind.ClipboardUser;
                case "settings":
                    return PackIconKind.Settings;
                case "edit_student":
                    return PackIconKind.UserEdit;
                case "view students":
                    return PackIconKind.AccountBoxMultiple;
                default: return PackIconKind.Null;
            }
        }
    }
}
