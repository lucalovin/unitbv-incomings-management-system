﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using TimetableReading;

namespace Idk.XAML
{
    /// <summary>
    /// Interaction logic for ViewTimetable.xaml
    /// </summary>
    public partial class ViewTimetable : Page
    {
        public String StudentName { get; set; }
        public static ObservableCollection<SubjectInfo> Courses { get; set; }
        private Student Student;
        private static DataGrid grid;
        private static ViewTimetable instance { get; set; }
        public ViewTimetable(Student st)
        {
            instance = this;
            TaskBar.StudentName = st.FullName;
            Student = st;
            Courses = new ObservableCollection<SubjectInfo>();
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += ViewTimetable_Loaded;
        }

        private void ViewTimetable_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            grid = timetable;
            foreach(SubjectInfo si in Student.AttendingCourses)
            {
                Courses.Add(si);
            }
        }

        public static void OnPrintClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            System.Windows.Controls.PrintDialog Printdlg = new System.Windows.Controls.PrintDialog();
            if ((bool)Printdlg.ShowDialog().GetValueOrDefault())
            {
                Size pageSize = new Size(Printdlg.PrintableAreaWidth, Printdlg.PrintableAreaHeight);
                // sizing of the element.
                grid.Measure(pageSize);
                grid.Arrange(new Rect(5, 5, pageSize.Width, pageSize.Height));
                Printdlg.PrintVisual(grid, "Student Timetable");
            }
        }

        public static void OnEditClicked(object sender, RoutedEventArgs e)
        {
            instance.NavigationService.Navigate(new Student_add_2nd(instance.Student));
        }
    }
}
