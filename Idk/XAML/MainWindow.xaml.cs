﻿using MaterialDesignThemes.Wpf;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Idk
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {   
        public string Password { set; get; }
        public string Email { set; get; }
        public MainWindow()
        {
            InitializeComponent();
            Unloaded += MainWindow_Unloaded;
        }

        private void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            string username = input_email.Text;
            string pass = input_password.Password;
            try
            {
                var addr = new System.Net.Mail.MailAddress(username);
            }
            catch
            {
                input_email.Focus();
                err_group.Visibility = Visibility.Visible;
                Thickness margin = err_group.Margin;
                margin.Bottom = -30;
                err_group.Margin = margin;
                error_msg.Text = "E-mail incorrect. Please try again.";
                TextFieldAssist.SetUnderlineBrush(input_email, Brushes.Red);
                return;
            }
            btn_login.IsEnabled = false;
            spinner_login.IsEnabled = true;
            spinner_login.Opacity = 100;
            spinner_login.Visibility = Visibility.Visible;
            Task<bool> auth = Task.Run(() => ClientHandler.authenticateAsync(username, pass));
            try
            {
                if (auth.Result == true)
                {
                    Console.WriteLine("Auth of [#" + ClientHandler.getClient().ClientID + "] " + ClientHandler.getClient().Email + " was successful.");
                    Home h = new Home();
                    h.Show();
                    this.Close();
                }
                else
                {
                    err_group.Visibility = Visibility.Visible;
                    error_msg.Text = "E-mail or password were incorrect. Please try again.";
                    Console.WriteLine("Wrong username/password. Auth failed.");
                    spinner_login.IsEnabled = false;
                    spinner_login.Opacity = 0;
                    spinner_login.Visibility = Visibility.Collapsed;
                    btn_login.IsEnabled = true;
                }
            }
            catch
            {
                string msgtext = "Server connection could not be established. Please try again.";
                string txt = "Connection Failed";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxResult result = MessageBox.Show(msgtext, txt, button, MessageBoxImage.Error);
                Console.WriteLine("Server error occured.");
                spinner_login.IsEnabled = false;
                spinner_login.Opacity = 0;
                spinner_login.Visibility = Visibility.Collapsed;
                btn_login.IsEnabled = true;
            }
        }

        private void beginDrag(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(e.ChangedButton == System.Windows.Input.MouseButton.Left)
            {
                DragMove();
            }
        }

        private void exitWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Input_password_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Enter)
            {
                Login_Click(sender, e);
            }
        }
    }
}
