﻿using System;
using System.Collections.Generic;
using TimetableReading;

namespace Idk
{
    public class Student : Client
    {
        public int Year { get; set; }
        public string Specialization { get; set; }
        public string Faculty { get; set; }
        public string Groupname { get; set; }
        public string Country { get; set; }
        public string UniverityOfOrigin { get; set; }
        public string Teacher { get; set; }
        public string DateOfBirth { get; set; }
        public string University { get; set; }
        public bool Modified { get; set; }
        public Dictionary<string, int> Courses { get; set; } = new Dictionary<String, int>();
        public List<SubjectInfo> AttendingCourses { get; set; } = new List<SubjectInfo>();

        public Student() : base() { }

        public Student(String fullname, int id, int age, int gender, String addr, int rank, int year) : base(fullname, id, age, gender, addr, rank)
        {
            Year = year;
        }
    }
}
