﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Idk
{
    class StudentHandler
    {
        //private static Dictionary<int, Student> Students = new Dictionary<int, Student>();
        private static SortedDictionary<int, Student> Students = new SortedDictionary<int, Student>();
        
        public static void addStudent(Student student)
        {
            Students.Add(student.ClientID == 0 ? student.ClientID + 1 : student.ClientID, student);
        }

        public static SortedDictionary<int, Student> getStudents()
        {
            return Students;
        }

        public static Student GetStudentByID(int studentID)
        {
            try {
                Students.TryGetValue(studentID, out Student st);
                return st;
            } catch (ArgumentNullException) {
                Console.WriteLine("Student " + studentID + " not found.");
                return null;
            }
        }

        public static bool SaveStudent(Student st, bool NewEntry)
        {
            if(st != null) {
                try
                {
                Task<bool> update = Task.Run(() => ClientHandler.UpdateRemote(JsonConvert.SerializeObject(st), false));

                    if (update.Result)
                    {
                        if (NewEntry) { addStudent(st); }
                        return true;
                    }
                }
                catch
                {
                    Console.WriteLine("Server error.");
                }
            }
            return false;
        }

        public static bool RemoveStudent(Student st)
        {
            if (st != null)
            {
                try
                {
                    Task<bool> update = Task.Run(() => ClientHandler.UpdateRemote(JsonConvert.SerializeObject(st), true));
                    if (update.Result)
                    {
                        Students.Remove(st.ClientID);
                        return true;
                    }
                }
                catch
                {

                }
            }
            return false;
        }
    }
}
