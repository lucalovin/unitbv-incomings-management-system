﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace TimetableReading
{
	public class Reader
	{
		private string _filePath = TimetableHelper.TimeTableLocation;
		private Dictionary<string, List<SubjectInfo>> _result = new Dictionary<string, List<SubjectInfo>>();

		public Reader()
		{
            Read();
 		}

		public void Read()
		{
			using (var stream = File.Open(_filePath, FileMode.Open, FileAccess.Read)) {
				using (var reader = ExcelReaderFactory.CreateReader(stream)) {
					var result = reader.AsDataSet();
					var table = result.Tables[0];

					bool bIecFound = false;
					int iYear = 0;
					string strGroup = String.Empty;
					string strSpecialization = String.Empty;
					string strSubGroup = String.Empty;
					for (int i = 3; i < table.Rows.Count; i++) {
						string strParity = "Odd";
						var items = table.Rows[i].ItemArray;
						if (bIecFound == true && !items[1].ToString().Equals("IEC") && !items[1].ToString().Equals(String.Empty)) {
							break;
						}
						if (!items[0].ToString().Equals(string.Empty)) {
							iYear = Int32.Parse(items[0].ToString());
						}
						if (!items[1].ToString().Equals(string.Empty)) {
							strSpecialization = items[1].ToString();
						}
						if (strSpecialization.Equals("IEC") && bIecFound == false) {
							bIecFound = true;
						}
						if (!items[2].ToString().Equals(string.Empty)) {
							strGroup = items[2].ToString();
						}
						if (!items[3].ToString().Equals(string.Empty)) {
							strSubGroup = items[3].ToString();
						} else {
							strParity = "Even";
						}

						if (strSpecialization == "IEC") {
							for (int j = 4; j < items.Count(); j++) {
								if (!items[j].ToString().Equals(String.Empty)) {
									string[] subjectDetails = items[j].ToString().Split(',');
									string strKey = subjectDetails[0].Trim(' ');
									string strType = subjectDetails[1].Trim(' ');
									string strRoom = subjectDetails[2].Trim(' ');
									string strProf = subjectDetails[3].Trim(' ');
									string strDay = GetDayByIndex(j);
									string strHours = GetHoursByIndex(j);
									SubjectInfo subjectInfo = new SubjectInfo(strKey, iYear, strType, strRoom, strProf, strDay, strHours, strParity, strGroup, strSubGroup);
									if (!_result.ContainsKey(strKey)) {
										_result.Add(strKey, new List<SubjectInfo>());
										_result[strKey].Add(subjectInfo);
									} else {
										_result[strKey].Add(subjectInfo);
									}
								}
							}
						}
 					}
                    _result.Remove("Pedag");
					_result.Remove("Lb.fr.");
				}
			}
		}

		private string GetDayByIndex(int i)
		{
			if (i >= 4 && i <= 10) {
				return "MONDAY";
			} else if (i >= 11 && i <= 17) {
				return "TUESDAY";
			} else if (i >= 18 && i <= 24) {
				return "WEDNESDAY";
			} else if (i >= 25 && i <= 31) {
				return "THURSDAY";
			} else if (i >= 32 && i <= 38) {
				return "FRIDAY";
			} else {
				return String.Empty;
			}
		}

		private string GetHoursByIndex(int i)
		{
			i = i % 7;
			switch (i) {
				case 4:
					return "8.00-9.50";
				case 5:
					return "10.00-11.50";
				case 6:
					return "12.00-13.50";
				case 0:
					return "14.00-15.50";
				case 1:
					return "16.00-17.50";
				case 2:
					return "18.00-19.50";
				case 3:
					return "20.00-21.50";
				default:
					return String.Empty;
			}
		}

		public Dictionary<string, List<SubjectInfo>> GetResult()
		{
			return _result;
		}

		public Dictionary<string, List<SubjectInfo>> GetSpecificSubjectsDictionary(List<string> list)
		{
			return _result.Where(index => list.Contains(index.Key)).ToDictionary(index => index.Key, index => index.Value);
		}
	}
}
