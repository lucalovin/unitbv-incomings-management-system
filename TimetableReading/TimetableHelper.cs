﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace TimetableReading
{
    public class TimetableHelper
    {
        private static readonly string TimeTableCacheName = "cache";
        private static readonly string TimeTableTempLocation = TimeTableCacheName + "/orar_iesc.tmp";
        public static readonly string TimeTableLocation = TimeTableCacheName + "/orar_iesc.xls";
        public static bool Status { get; private set; } = false;
        public static bool DownloadStatus { get; private set; } = false;

        public static void GetLatestTimetable()
        {
            using (WebClient client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                Directory.CreateDirectory(TimeTableCacheName);
                try
                {
                    client.DownloadFileCompleted += Client_DownloadFileCompleted;
                    client.DownloadProgressChanged += Client_DownloadProgressChanged;
                    client.DownloadFileAsync(new Uri("https://bamalex.com/api/getLatestTimetable.php"), TimeTableTempLocation);
                    DownloadStatus = true;
                }
                catch
                {
                    Console.WriteLine("Server-side error occured. Cannot download latest timetable.");
                }

            }
        }

        private static void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            //Task.Factory.StartNew(() => .Enqueue(message));
            Console.WriteLine("Downloading new timetable progress: " + e.ProgressPercentage + "% (" + e.BytesReceived + "/" + e.TotalBytesToReceive + ")");
        }

        private static void Client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                FileInfo Cached = new FileInfo(TimeTableLocation);
                FileInfo NewFile = new FileInfo(TimeTableTempLocation);
                if (Cached.Exists && NewFile.Exists && NewFile.Length != Cached.Length && NewFile.Length > 0)
                {
                    Console.WriteLine("Existing timetable updated.");
                    File.Delete(TimeTableLocation);
                    File.Move(TimeTableTempLocation, TimeTableLocation);
                }
                else if (Cached.Exists && NewFile.Exists && NewFile.Length == Cached.Length)
                {
                    Console.WriteLine("Timetable already up to date.");
                    File.Delete(TimeTableTempLocation);
                }
                if (!Cached.Exists && NewFile.Exists && NewFile.Length > 0)
                {
                    Console.WriteLine("New timetable downloaded.");
                    File.Move(TimeTableTempLocation, TimeTableLocation);
                }
                Status = true;
            }
            else
            {
                File.Delete(TimeTableTempLocation);
                Console.WriteLine("Server error: " + e.Error);
                Status = false;
                DownloadStatus = false;
            }
        }
    }
}
