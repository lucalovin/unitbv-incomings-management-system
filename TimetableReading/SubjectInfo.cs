﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimetableReading
{
	public class SubjectInfo
	{
		public int Year { get; set; }
		public string Room { get; set; }
		public string Type { get; set; }
		public string Professor { get; set; }
		public string Day { get; set; }
		public string Hours { get; set; }
		public string Parity { get; set; }
		public string Group { get; set; }
		public string Semigroup { get; set; }
        public string Key { get; set; }
		public SubjectInfo(string strKey, int iYear, string strType, string strRoom, string strProf, string strDay, string strHours, string strParity, string strGroup, string strSemigroup)
		{
            Key = strKey;
			Year = iYear;
			Type = strType;
			Room = strRoom;
			Professor = strProf;
			Day = strDay;
			Hours = strHours;
			Parity = strParity;
			Group = strGroup;
			Semigroup = strSemigroup;
		}
	}
}